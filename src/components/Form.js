import React from "react";

const api__key = "94ef65fbbe9d2de0910da483f8c9e624";

const Form = () => {

	const [weather, setWeather] = React.useState();

	

	
	React.useEffect(() => {
		async function gettingWeather(event) {
			event.preventDefault();
			const api__url = await fetch(`api.openweathermap.org/data/2.5/weather?q=London&appid=${api__key}`)
			const data = await api__url.json();
			console.log(data); 
		}
	});


	return (
		<form >
			<input type="text" 
			name="city" 
			placeholder="City name" 
			value={weather}
			/>
			<button>Get Weather!</button>
		</form>
	)
}

export default Form;